From: Simon McVittie <smcv@collabora.com>
Date: Wed, 5 Jun 2019 13:36:52 +0100
Subject: gvfsdaemon: Only accept EXTERNAL authentication

EXTERNAL is the mechanism recommended in the D-Bus Specification for
all platforms where it is supported (including Linux, *BSD, Solaris
and Hurd), and is the only mechanism allowed by the session or system
dbus-daemon in their default configurations. It is considerably simpler
than DBUS_COOKIE_SHA1 and relies on fewer assumptions.

Signed-off-by: Simon McVittie <smcv@collabora.com>
Applied-upstream: 1.40.2, commit:a0da5f16feda323c29850c495acd86dfc8fbb262
---
 daemon/gvfsdaemon.c | 17 +++++++++++++++++
 1 file changed, 17 insertions(+)

diff --git a/daemon/gvfsdaemon.c b/daemon/gvfsdaemon.c
index be148a7..0946f41 100644
--- a/daemon/gvfsdaemon.c
+++ b/daemon/gvfsdaemon.c
@@ -239,6 +239,22 @@ name_vanished_handler (GDBusConnection *connection,
   daemon->lost_main_daemon = TRUE;
 }
 
+/*
+ * Authentication observer signal handler that rejects all authentication
+ * mechanisms except for EXTERNAL (credentials-passing), which is the
+ * recommended authentication mechanism for AF_UNIX sockets.
+ */
+static gboolean
+allow_mechanism_cb (GDBusAuthObserver *observer,
+                    const gchar *mechanism,
+                    G_GNUC_UNUSED gpointer user_data)
+{
+  if (g_strcmp0 (mechanism, "EXTERNAL") == 0)
+    return TRUE;
+
+  return FALSE;
+}
+
 /*
  * Authentication observer signal handler that authorizes connections
  * from the same uid as this process. This matches the behaviour of a
@@ -298,6 +314,7 @@ g_vfs_daemon_init (GVfsDaemon *daemon)
   daemon->conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, NULL);
   g_assert (daemon->conn != NULL);
   daemon->auth_observer = g_dbus_auth_observer_new ();
+  g_signal_connect (daemon->auth_observer, "allow-mechanism", G_CALLBACK (allow_mechanism_cb), NULL);
   g_signal_connect (daemon->auth_observer, "authorize-authenticated-peer", G_CALLBACK (authorize_authenticated_peer_cb), NULL);
 
   daemon->daemon_skeleton = gvfs_dbus_daemon_skeleton_new ();
